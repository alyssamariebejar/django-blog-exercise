from django.apps import AppConfig


class ThinkwebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'thinkWeb'
