#adds additional field

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserRegisterForm(UserCreationForm):
    email = forms.EmailField() #adds email field to forms

    class Meta:
        model = User #save to user model
        fields = ['username', 'email', 'password1', 'password2'] #fields needed in the form