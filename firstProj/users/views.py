from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm #syntax for using the builtin form
from django.contrib import messages
from . forms import UserRegisterForm

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account successfully created for {username}, you can now login')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/userRegister.html', {'form': form})
